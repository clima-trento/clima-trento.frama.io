# clima-adige.frama.io

Applicazione di sondaggio basata sul metodo Q. Codice sorgente originale dell'applicazione su https://github.com/shawnbanasick/eq-web-sort. Vedere anche https://github.com/shawnbanasick/eq_web_configurator (configuratore semi-automatico e istruzioni per l'implementazione). Versione 2.0.0 (23/11/2022).

Applicazione implementata su https://clima-adige.frama.io/, per il territorio del bacino dell’Adige.

Per saperne di più sul progetto: https://clima-adige.frama.io/progetto.
